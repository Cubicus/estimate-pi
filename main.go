package main

import (
	"fmt"
	"math"
	"math/rand"
	"runtime"
	"time"
)

func main() {
	nCPU := runtime.NumCPU()
	fmt.Println("nCPU =", nCPU)

	n := 10000000

	startTime := time.Now()
	pi := estimatePi(n)
	endTime := time.Now()
	fmt.Printf("Pi: %f, Diff: %f \n", pi, math.Pi-pi)
	fmt.Printf("Time: %d ms \n", endTime.Sub(startTime).Milliseconds())

	startTime = time.Now()
	pi = estimatePiConcurrent(n, nCPU)
	endTime = time.Now()
	fmt.Printf("Pi: %f, Diff: %f \n", pi, math.Pi-pi)
	fmt.Printf("Time: %d ms \n", endTime.Sub(startTime).Milliseconds())

	startTime = time.Now()
	pi = estimateAVGPiConcurrent(n, nCPU)
	endTime = time.Now()
	fmt.Printf("Pi: %f, Diff: %f \n", pi, math.Pi-pi)
	fmt.Printf("Time: %d ms \n", endTime.Sub(startTime).Milliseconds())
}

func estimatePi(n int) float64 {
	circlePoints := 0.0
	totalPoints := 0.0
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < n; i++ {
		x := r.Float64()
		y := r.Float64()
		distance := (x * x) + (y * y)
		if distance <= 1 {
			circlePoints++
		}
		totalPoints++
	}

	return 4 * circlePoints / totalPoints
}

func estimateAVGPiConcurrent(n int, c int) float64 {
	piResultsChan := make(chan float64, c)
	for i := 0; i < c; i++ {
		go func(n int, piResultsChan chan<- float64) {
			piResultsChan <- estimatePi(n)
		}(n/c, piResultsChan)
	}

	piSum := 0.0
	for j := 0; j < c; j++ {
		piSum += <-piResultsChan
	}
	return piSum / float64(c)
}

func estimatePiConcurrent(n int, c int) float64 {
	type PartialResult struct {
		CirclePoints float64
		TotalPoints  float64
	}

	partialResultChan := make(chan PartialResult, c)
	for j := 0; j < c; j++ {
		go func(n int, circlePointsChan chan<- PartialResult) {
			r := rand.New(rand.NewSource(time.Now().UnixNano()))
			circlePoints := 0.0
			totalPoints := 0.0
			for i := 0; i < n; i++ {
				x := r.Float64()
				y := r.Float64()
				distance := (x * x) + (y * y)
				if distance <= 1 {
					circlePoints++
				}
				totalPoints++
			}
			partialResultChan <- PartialResult{
				CirclePoints: circlePoints,
				TotalPoints:  totalPoints,
			}
		}(n/c, partialResultChan)
	}

	circlePoints := 0.0
	totalPoints := 0.0
	for i := 0; i < c; i++ {
		result := <-partialResultChan
		circlePoints += result.CirclePoints
		totalPoints += result.TotalPoints
	}
	return 4 * circlePoints / totalPoints
}
